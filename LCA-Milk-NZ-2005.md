# First Life Cycle Assessment of Milk Production from New Zealand Dairy Farm Systems

LCA is a tool for which aides in the evaluation of environmental impacts of a product by quantifying and analyzing production from all life cycle stages.

### Functional Unit
One unit of production (kg of milk)

### System boundaries
The studied system included the processes up to the farm (production and delivery of crop and pasture inputs, production of feed supplement, off-farm pasture production for the replacement of cows), as well as the production of milk on the farm Basset-Mens, Ledgard and Carran 259 including on-farm pasture production, herd management and milk extraction (Ledgard et al., 2003).

Impacts were allocated between the co-products milk and meat (85:15) according to a biological causality, using the same equation as Cederberg and Mattsson (2000) but with NZ data.

### Impact Methods
- Global Warming Potential
- Eutrophication Potential
- Acidification Potential

### Results
Impacts for New Zealand compared to European systems were lower across the board with a high milk productivity per ha.


# Future Research
- Van Der Werf and Petit, 2002 — value of whole farm LCA
- Cederberg, C., Mattsson, B., (2000) Life cycle assessment of milk production – a comparison of conventional and organic farming. Journal of Cleaner Production, 8, 49-60. -> Allocation by biological causality
- De Boer, I.J.M., (2003) Environmental impact assessment of conventional and organic milk production. Livestock Production Science, 80(1-2), 69-77. -> Difficult to harmonize LCA
