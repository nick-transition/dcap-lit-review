# Environmental impact of future milk supply chains in Sweden: a scenario study

Dairy has experienced significant changes in production and consumption over the past 30 years. LCA allows the assessment of a product's impacts from "cradle to grave" — accounting for impacts associated with both consumption and production. the major purpose of the study is to evaluate the potential environmental impacts of future dairy production in Sweden.

### Functional Unit
This study deviates from standard LCA seeking to compare impacts on the basis of milk required for production of a product (e.g. cheese).

### System boundaries
By using a scenario technique to evaluate environmental performance the reader gains a sense for how certain production practices affect the overall impact of milk production. Agricultural analysis in not included in this study.

### Impact Methods
- Energy Consumption
- Global Warming Potential
- Acidification Potential
- Eutrophication Potential
- Photochemical Oxidants

### Results
The demand for more and fresher milk products affects the environmental impact of the entire milk supply chain. Resulting in more frequent transportation in distribution, retailing, home transport and household wastage. Further, energy consumption at packaging seems to pllay a large role in the overall impact of milk production.


# Future Research
